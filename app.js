const btn = document.querySelector('#clicker');
// btn.onclick = alert('Hi!!');

btn.onclick = function(){
  console.log('You Clicked Me! Go away!');
}

btn.ondblclick = function(){
  console.log('Double click!');
}

function greet(){
  alert('Hey Buddy!')
}

const btnE = document.querySelector('#event');
btnE.onclick = function(){
  console.log('You clicked me again');
};

btnE.onclick = function(){
  console.log('Second time!!');
};

btnE.addEventListener('click', function(){
  alert('Clicked!!!!!!!!!!!!!!!!!!')
})

btnE.addEventListener('click', function(){
  console.log('Second Thing!!!!')
})

btnE.addEventListener('mouseover', function(){
  btnE.innerText = 'Stop Touching Me';
})

btnE.addEventListener('mouseout', function(){
  btnE.innerText = 'Click Me';
})

//events on multiple elements: 
const colors = [
  'red',
  'orange',
  'yellow',
  'green',
  'blue',
  'purple',
  'indigo',
  'violet'
];

const changeColor = function(){
  const h2 = document.querySelector('h2');
  h2.style.color = this.style.backgroundColor;
}

const container = document.querySelector('#boxes');

for(let color of colors){
  const box = document.createElement('div');
  box.style.backgroundColor = color;
  box.classList.add('box');
  container.appendChild(box);
  box.addEventListener('click', changeColor);
}

// document.body.addEventListener('keypress', function(e){
//   console.log(e);
// })

//key events 
const input = document.querySelector('#username');
input.addEventListener('keydown', function(e){
  console.log('Key down')
})
input.addEventListener('keyup', function(e){
  console.log('Key up')
})
input.addEventListener('keypress', function(e){
  console.log('Key press')
})

const addItemInput = document.querySelector('#addItem');
const itemsUL = document.querySelector('#items');
addItemInput.addEventListener('keypress', function(e){
  // console.log(e);
  if(e.key === 'Enter'){
    if(!this.value) return; //enter with keypress, return nothing
    //add a new item to List
    const newItemText = this.value;
    const newItem = document.createElement('li');
    newItem.innerText = newItemText; 
    itemsUL.appendChild(newItem);
    this.value ='';
  }
})


//Form Events & PreventDefault 
const creditCardInput = document.querySelector('#cc');
const termsCheckBox = document.querySelector('#terms');
const veggieSelect = document.querySelector('#veggie');

const form = document.querySelector('#signup-form');
form.addEventListener('submit', function(e){
  e.preventDefault();
  // alert("Submit the form");
  console.log('cc', creditCardInput.value);
  console.log('terms', termsCheckBox.checked); //.checked to return true false 
  console.log('veggie', veggieSelect.value);
  //able to send form data to db 
  //able to append something to page using form data
  
})


//Input & change Events 
console.log('cc', creditCardInput.value);
console.log('terms', termsCheckBox.checked); //.checked to return true false 
console.log('veggie', veggieSelect.value);

const formData = {};
for (let input of [creditCardInput, termsCheckBox, veggieSelect]){
  input.addEventListener('input', ({target}) => { //change 'input' to 'change'
    const {name, type, value, checked} = target;
    // console.log(e.target.name);
    formData[name] = type === 'checkbox' ? checked : value;
    console.log(formData);
  })
}
//'input' and 'change' work same but 
// input show result anytime we are typing
// change show result after enter or submit


//Another way:
const formData2 = {};
creditCardInput.addEventListener('input', function(e){
  console.log('CC change', e);
  formData2['cc'] = e.target.value;
})

veggieSelect.addEventListener('input', (e) => {
  console.log('Veggie', e);
  formData2['veggie'] = e.target.value;
})

termsCheckBox.addEventListener('input', function(e){
  console.log('CheckBox', e);
  formData2['terms'] = e.target.checked;
});


//Call Stack

const repeat = (str, times) => {
  let result = '';
  for(let i = 0; i < times; i++){
    result += str;
  }
  return result;
}

const scream = (str) => {
  return str.toUpperCase() + '!!!';
}

const getRantText = (phrase) => {
  let text = scream(phrase);
  let rant = repeat(text, 8);
  return rant;
};

const makeRant = (phrase, el) => {
  const h4 = document.createElement('h4');
  h4.innerText = getRantText(phrase);
  el.appendChild(h4);
};
console.log('HELLO');
makeRant("I don't hate anything", document.body);


//CALLBACK 
const btnCall = document.querySelector('#call');

// setTimeout(() => {
//   btnCall.style.transform = `translateX(100px)`;
//   setTimeout(() => {
//     btnCall.style.transform = `translateX(200px)`;
//     setTimeout(() => {
//       btnCall.style.transform = `translateX(300px)`;
//       setTimeout(() => {
//         btnCall.style.transform = `translateX(400px)`;
//         setTimeout(() => {
//           btnCall.style.transform = `translateX(500px)`;
//         }, 1000)
//       }, 1000)
//     }, 1000)
//   }, 1000)
// }, 1000)

//another way: 
const moveX = (element, amount, delay, callback) => {
  
  setTimeout(() => {
    element.style.transform = `translateX(${amount}px)`;
    if(callback) callback();
  }, delay);
};

moveX(btnCall, 100, 1000, () => {
  moveX(btnCall, 200, 1000, () => {
    moveX(btnCall, 300, 1000, () => {
      moveX(btnCall, 400, 1000, () => {
        moveX(btnCall, 500, 1000);
      }); 
    });
  });
});


// Promise 
const willGetYouADog = new Promise((resolve, reject) => {
  setTimeout(() => {
    const rand = Math.random();
    if (rand < 0.5) {
      resolve();
    }
    else {
      reject();
    }
  }, 5000);
});
  
willGetYouADog.then(() => { //resolve
  console.log('Yay we got a dob');
}) 
willGetYouADog.catch(() => { //reject
  console.log(':( No Dog');
})

//another way: 
const makeDogPromise = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const rand = Math.random();
      if (rand < 0.5) {
        resolve();
      }
      else {
        reject();
      }
    }, 5000);
  });
} 
  
makeDogPromise()
  .then(() => { //resolve
  console.log('Yay we got a dob');
}) 
  .catch(() => { //reject
  console.log(':( No Dog');
})

//Resolving/rejecting w/values
const fakeRequest = (url) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // const rand = Math.random()
      // if(rand <0.3){
      //   reject({status: 404});
      // }
      // else{
        const pages = {
          '/users': [
            { id: 1, username: 'Bilbo'},
            { id: 1, username: 'Hanna'},
          ],
          '/about' : 'This is the about page!'
        };
        const data = pages[url]
        if(data){
          resolve({status: 200, data});
        }
        else {
          reject({status: 404});
        }
    }, 1000);
  });
};
fakeRequest('/about')
  .then((res) => {
    console.log('Status Code', res.status)
    console.log('Data', res.data)
    console.log('Request worked!');
  })
  .catch((res) => {
    console.log('Request failed');
  })

  fakeRequest('/about')
  .then((res) => {
    console.log('Status Code', res.status)
    console.log('Data', res.data)
    console.log('Request worked!');
  })
  .catch((res) => {
    console.log('Request failed');
  })



