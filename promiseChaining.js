//The Delights of Promise Chaining 
const fakeRequest = (url) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const pages = {
                '/users': [
                    { id: 1, username: 'Bilbo' },
                    { id: 5, username: 'Hanna' },
                ],
                '/users/1': {
                    id:     1,
                    username: 'Bilbo',
                    upvotes: 360,
                    city: 'Lisbon',
                    topPostId: 454321
                },
                '/users/5' : {
                    id:     5,
                    username: 'Hanna',
                    upvotes: 571,
                    city: 'Honolulu'
                }, 
                '/posts/454321': {
                    id: 454321,
                    title: 'Ladies & Gentlemen, may I introduce my pet pig, Hamlet'
                },
                '/about': 'This is the about page!'
            };
            // console.log(url);
            const data = pages[url]
            if (data) {
                resolve({ status: 200, data }); //resolve with a value
            }
            else {
                reject({ status: 404 }); //reject with a value
            }
        }, 1000);
    });
};

//method 1:
// fakeRequest('/users').then((res) => {
//     const id = res.data[0].id;
//     fakeRequest(`/users/${id}`).then((res) => {
//         const postId = res.data.topPostId;
//         fakeRequest(`/posts/${postId}`).then((res) => {
//             console.log(res);
//         });
//     })
// })

//method2:
fakeRequest('/users')
    .then((res) => {
        console.log(res);
        const id = res.data[0].id;
        return fakeRequest(`/users/${id}`);
    })
    .then((res) => {
        console.log(res);
        const postId = res.data.topPostId;
        return fakeRequest(`/postzcscs/${postId}`); 
    })
    .then((res) => {
        console.log(res);
    })
    .catch((err) => {
    console.log('Oh no', err)
});
