alert("It's working!")

console.log(3+3);
console.error('OH NOO!!!');
console.log( 3 +4, 'hello', true);

if(1 === 1 ){
    console.log("It's true!"); //the code will run 
}
if(1 !== 1 ){
    console.log("It's true!"); //the code won't run
}


//Exmaple 2:
let rating = 3;
if (rating === 3){
    console.log("You are a supperstar");
}


//Example 1 
let rating = 2;
if (rating === 3){
    console.log("You are a supperstar");
}
else if (rating ===2){
    console.log('You are better');
}
else if (rating === 1){
    console.log("You are awesome");
}
else {
    console.log('INVALID RATING!');
}

//Exmaple 2:
let highScore = 1430;
let userScore = 1600;
if(userScore > highScore){
    console.log(`Congrats, you have the new high score of ${userScore}`);
}
else {
    console.log(`Good Game. Your score of ${userScore} did not beat the high score of ${highScore}`);
}


Example:
let loggedInUser = '';
if (!loggedInUser){
    console.log('Get out of here!');
}

let flavor = 'watermelon';
// if(flavor !== 'grape' && flavor !== 'cherry'){
//     console.log("we don't have that flavor!");
// }
//equal:
if(!(flavor === 'grape' || flavor === 'cherry')){
    console.log('We only have grape and cherry');
}


const student = {
    firstName: 'David',
    lastName: 'Jones',
    strengths: ['Music', 'Art'],
    exams: {
      midterm: 92, 
      final: 88
    }
  }
const avg = (student.exams.midterm + student.exams.final)/2; // avg = 90

const gameBoard = [
  [4, 32, 8, 4],
  [64, 8, 32, 2],
  [8, 32, 16, 4],
  [2, 8, 4, 2]
];

let totalScore =0;
for (let i = 0; i < gameBoard.length; i++){
  let row = gameBoard[i];
  console.log(row);
  for (let j = 0; j < row.length; j++){
    totalScore += row[j]
    console.log(totalScore);
  }
}

let i = 0;
while (i <= 5){
  console.log(i);
  i++;
};


function isValidPassword(password, username){
  if (password.length >= 8 && password.indexOf(' ') === -1 && password.indexOf(usrname) === -1){
    return true;
  }
  return false; 
}
isValidPassword('asfg2', 'asd');
isValidPassword('wehgosiew', 'wersa');
isValidPassword('hienhoa88', 'paulmoreland');

function average(arr){
  let total = 0;
  for (let i = 0; i <= arr.length; i++){
      total += arr;
  }
  let average = total/arr.length; 
  return average;
}
average([2, 4, 5, 6, 7, 8])

function avg(arr){
  let total = 0;
  for(let i = 0; i <=arr.length; i++){
    total += i;
  }
  return total/arr.length;
}
avg([3,5,6,6,3,2,1,8]); //4.5


function multiply(x,y){
  return x * y;
}
const thing = {
  doSomthing: multiply
}



function grumpus(){
  alert("Go Away");
}
const btn = document.querySelector('button');
// btn.addEventListener('click', grumpus);
btn.addEventListener('click', function() {
  alert("Why did you click me!?");
});

class Person {
  firstName = 'first';
  lastName = 'last';

  getFirstName = (message) => {
    console.log(message);
    console.log(`${this.firstName} ${this.lastName}`)
  }

  nonArrowVersion(message) {
    console.log(message);
    console.log(`${this.firstName} ${this.lastName}`)
  }
}

var person = new Person();
person.getFirstName('first call');
const func = person.getFirstName;
func('second call');

person.nonArrowVersion('thrid call');
const func2 = person.nonArrowVersion;
func2('fourth call');


//Change Google page with some JavaScript: 
const myImg = document.createElement('img');
myImg.src ='https://www.thoughtco.com/thmb/NDCCAJXZxTnLO7l1HbT7Zkp0D9I=/768x0/filters:no_upscale():max_bytes(150000):strip_icc()/lotus-flower-828457262-5c6334b646e0fb0001dcd75a.jpg';

myImg.style.width = '200px';
document.body.append(myImg);
myImg.style.transition = 'all 2s';

const sheet = new CSSStyleSheet();
sheet.replaceSync('* {transition: all 2s}');
document.adoptedStyleSheets = [sheet];

const allEls = document.body.children;

setInterval(() => {
    for(let el of allEls){
        const rotation = Math.floor(Math.random() * 360);
        const x = Math.floor(document.body.clientWidth * Math.random())
        const y = Math.floor(document.body.clientheight * Math.random())
        el.style.transform = `translate(${x}px, ${y}px) rotate(${rotation}deg`;
    }
    
}, 2000)


//play around Google page:
const obj = document.querySelector('#gbqfbb');
console.dir(obj);
obj.value; 
obj.addEventListener('mouseover', function(){
  alert("Good Luck Hien") } )



//Chaning Multiple Elements 
const allLis = document.querySelectorAll('li');

for (let i = 0; i <allLis.length; i++){
  // console.log(allLis[i].innerText);
  allLis[i].innerText = "We are the Champions!"
}

for(let li of allLis){
  li.innerHTML = 'We are <b>The Champion</b>';
}


//Altering Styles 
const allLis = document.querySelectorAll('li');
const colors = ['red', 'organe', 'yellow', 'green', 'blue', 'purple'];

allLis.forEach((li, i) => {
  const color = colors[i];
  console.log(li,i);
  li.style.color = color;
})

// for(var li of allLis) {

// }


