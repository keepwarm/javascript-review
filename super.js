class Cat {
    constructor(name, age){
        this.name = name;
        this.age = age;
    }
    eat(){
        return `${this.name} is eating!`;
    }
    meow(){
        return 'meowww!!';
    }
}

class Dog {
    constructor(name, age){
        this.name = name,
        this.age = age;
    }
    eat(){
        return `${this.name} is eating!`;
    }
    bardk(){
        return 'Wooff!';
    }
}

//const wyatt = new Dog('Wyatt', 13); //Dog {name; 'Wyatt', age: 13}



//Other way: 
class Pet {
    constructor(name, age){
        this.name = name;
        this.age = age;
    }
    eat(){
        return `${this.name} is eating!`;
    }
};

class Cat extends Pet {
    meow(){
        return 'meowww!!';
    }
}

class Dog extends Pet {
    bardk(){
        return 'Wooff!';
    }
    eat(){
        return `${this.name} scarfs his food`;
    }
}
    
//const wyatt = new Dog('Wyatt', 13); //Dog {name: 'Wyatt', age: 13} 
//wyatt.eat();//Wyatt scarf his food 
//const monty = new Cat('Monty', 9); //Cat {name:'Monty', age: 9}
//monty.eat(); //'Monty is eating"
//monety.meow(); //"meowww!!"


//'super' key word
class Pet {
    constructor(name, age){
        console.log('In Pet constructor');
        this.name = name;
        this.age = age;
    }
    eat(){
        return `${this.name} is eating!`;
    }
};

class Cat extends Pet {
    constructor(name, age, livesLeft = 9){
        console.log('In cat constructor')
        // this.name = name;
        // this.age = age;
        super(name,age); //reuse the function - reference from parent function
        this.livesLeft = livesLeft;
    }
    meow(){
        return 'meowww!!';
    }
}

class Dog extends Pet {
    bardk(){
        return 'Wooff!';
    }
    eat(){
        return `${this.name} scarfs his food`;
    }
}