const firstReq = new XMLHttpRequest();
    firstReq.addEventListener('load', function(){
      console.log('First Request worked!');
      const data = JSON.parse(this.responseText);
      const filmURL = data.results[0].films[0];
      const filmReq = new XMLHttpRequest();
      filmReq.addEventListener('load', function(){
        console.log('Second Request worked!!');
        const filmData = JSON.parse(this.responseText);
        console.log(filmData);
      });
      filmReq.addEventListener('error', function(){
        console.log('Error!', e);
      });
      filmReq.open('GET', filmURL);
      filmReq.send();
    });
    firstReq.addEventListener('error', (e) =>{
      console.log('ERROR!!!');
    });
    firstReq.open('GET', 'http://swapi.co/api/planets/');
    firstReq.send();
    console.log('Request Sent!');

//Using fetch:
fetch('https://swapi.dev/api/planets/')
    .then((response) => {
        if (!response.ok)
            throw new Error(`Status Code Error: ${response.status}`);
        return response.json();
    })
    .then((data) => {
        console.log('Fetch all planets(first 10)');
        for (let planet of data.results) {
            console.log(planet);
            console.log(planet.name);
        }
        const nextURL = data.next;
        return fetch(nexURL);
    })
    .then((response) => {
        if (!response.ok)
            throw new Error (`Status Code Error: ${response.status}`);
        return response.json();
    })
    .then((data) => {
        console.log('Fetched first film, based off of first planet');
        console.log(data.title);
    })
    .catch((err) => {
        console.log('Something went wrong with fetch!');
        console.log(err);
    });

//Cleaning up the code with Fetch 
const checkStatusAndParse = (response) => {
    if (!response.ok) throw new Error (`Status Code Error: ${response.status}`);
    return response.json();
}

const printPlanets = (data) => {
    console.log('Loaded 10 more planets..');
    for(let planet of data.results){
        console.log(planet.name);
    }
    return Promise.resolve(data.next); //.next is key/property of data 'next' url in data
};

const fetchNextPlanets = (url = 'https://swapi.dev/api/planets/') => {
    return fetch(url);
}

fetchNextPlanets()
    .then(checkStatusAndParse)
    .then(printPlanets)
    .then(fetchNextPlanets)
    .then(checkStatusAndParse)
    .then(printPlanets)
    .then(fetchNextPlanets)
    .then(checkStatusAndParse)
    .then(printPlanets)
    .catch((err) => {
        console.log('Something went wrong with fetch!');
        console.log(err);
    });

//AN EVEN BETTER WAY: AXIOS 
const fetchNextPlanets = (url = 'https://swapi.dev/api/planets') => {
    console.log(url);
    return axios.get(url);
};
const printPlanets = ({data}) => {
    console.log(data);
    for(let planet of data.results){
        console.log(planet.name);
    }
    return Promise.resolve(data.next);
};
fetchNextPlanets()
    .then(printPlanets)
    .then(fetchNextPlanets)
    .then(printPlanets)
    .then(fetchNextPlanets)
    .then(printPlanets)
    .catch((err) => {
        console.log('Error!!',err);
    });

