class Color {
    constructor(r,g,b, name){
        console.log('Inside contructor!');
        console.log(r,g,b);
        this.r = r;
        this.g = g;
        this.b = b;
        this.colorName = name
    }
    rgb(){
        const { r, g, b} = this;
        return `rgb(${r}, ${g}, ${b})`;
    }
    rgba(a=1){
        return `rgba (${r}, ${g}, ${b}, ${a})`;
    }
    hex(){
        const { r, g, b} = this;
        return (
            '#' +  ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)
        );
    }
}

const red = new Color(255,67,89, 'tomato');
const white = new Color(255,255,255, 'white');


//other way:
class Color {
    constructor(r,g,b, name){
        console.log('Inside contructor!');
        console.log(r,g,b);
        this.r = r;
        this.g = g;
        this.b = b;
        this.colorName = name
    }
    innerRGB(){
        const { r, g, b} = this;
        return `(${r}, ${g}, ${b})`;
    }
    rgb(){
        return `rgb(${this.innerRGB()})`;
    }
    rgba(a=1){
        return `rgb(${this.innerRGB()}, ${a})`;
    }
    hex(){
        const { r, g, b} = this;
        return (
            '#' +  ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)
        );
    }
}

const red = new Color(255,67,89, 'tomato');
const white = new Color(255,255,255, 'white');

//white.rgba()
//red.rgba()
//red.rgba(0.4)