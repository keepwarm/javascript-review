//async keyword:
// async function add(x,y) {
//     if(typeof x !== 'number' || typeof y !== 'number'){
//         throw 'X and Y must be numbers!';
//     }
//     return x + y;
// }

// function add(x,y){
//     return new Promise((resolve, reject) => {
//         if (typeof x !== 'number' || typeof y !== 'number'){
//             reject ('X and Y must be numbers');
//         }
//         resolve( x + y);
//     });
// }

// add(6,'7')
//     .then((val) => {
//         console.log('PROMISE RESOLVED WITH: ', val);
//     })
//     .catch((err) => {
//         console.log('PROMISE REJECTED WITH: ', err);
//     })


//await keyword: 
function getPlanets(){
    return axios.get('https://swapi.dev/api/planets/');
}
getPlanets().then((res) => {
    console.log(res.data);
});
//this is the same as: 
async function getPlanets(){
    const res = await axios.get('https://swapi.dev/api/planets/');
    console.log(res.data); //only runs once the previous line is complete (the axios promise is resolved)
}
getPlanets();


//Error handling in Async Functions: 
async function getPlanets(){
    const res = await axios.get('https://swapi.dev/api/planets/');
    console.log(res.data);
}
getPlanets().catch((err) => {
    console.log('In catch!!');
    console.log(err);
})
//this is the same as: 
async function getPlanets(){
    try{
        const res = await axios.get('https://swapi.dev/api/planets/');
        console.log(res.data);
    }
    catch(e){
        console.log('In Catch!',e);
    }
}
getPlanets();

